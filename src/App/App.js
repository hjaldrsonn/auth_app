import React, { createContext, useReducer } from 'react';
import { HashRouter } from 'react-router-dom';
import Page from '../Page/Page';
import './App.css';

export const UserContext = createContext(null);

const loginReducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      return action.user;
    case "LOGOUT":
      return null
    default:
      return state;
  }
};

export default function App() {
  const [user, dispatch] = useReducer(loginReducer, null);

  return (
    <HashRouter>
      <UserContext.Provider value={{ user, dispatch }}>
        <Page />
      </UserContext.Provider>
    </HashRouter>
  )
}
