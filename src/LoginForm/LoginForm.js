import React, { useContext, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { UserContext } from '../App/App';

export default function LoginForm() {
  const [email, setEmail] = useState('');

  const { dispatch } = useContext(UserContext);

  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch({ type: "LOGIN", user: email });

    window.location.hash = "#/account";
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
  }

  return (
    <div id='loginForm'>
      <h1 className="text-center">Login</h1>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="loginEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={handleEmail} />
        </Form.Group>

        <Form.Group className="mb-3" controlId="loginPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>
        <Button variant="primary" type="submit">Login</Button>

      </Form>
    </div>
  )
}
