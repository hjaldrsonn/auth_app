import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { Routes, Route } from 'react-router-dom';
import { UserContext } from '../App/App';
import LoginForm from '../LoginForm/LoginForm';
import AccountPage from './AccountPage/AccountPage';

export default function Page() {
    const { user, dispatch } = useContext(UserContext);
    const logoutButton = <Button type="submit" id='logout' variant="secondary" size="sm" onClick={() => { dispatch({ type: "LOGOUT" }); }}>Logout</Button>;
    return (
        <div className='row' id='main-page'>
            <div className='col-sm-9 col-md-9 center-block'>
                {user ? logoutButton : null}

                <Routes>
                    <Route path={`/`} element={<AccountPage />} />
                    <Route path={`/login`} element={<LoginForm />} />
                    <Route path={`/account`} element={<AccountPage />} />
                </Routes>
            </div>
        </div>
    )
}
