import React, {useContext} from 'react';
import { Navigate } from 'react-router-dom';
import { UserContext } from '../../App/App';

export default function AccountPage() {
  const { user } = useContext(UserContext);

  if (!user) {
    return <Navigate to="/login" replace />;
  }

  return (
    <>
      <h1 className='text-center'>Account Page</h1>
      <h3>Hello, {user}</h3>
    </>
  )
}
